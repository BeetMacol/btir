package com.beetmacol.mc.btir.blocks;

import com.beetmacol.mc.btir.Main;
import net.minecraft.core.block.Block;
import net.minecraft.core.block.entity.TileEntity;
import net.minecraft.core.block.material.Material;
import net.minecraft.core.enums.EnumDropCause;
import net.minecraft.core.item.Item;
import net.minecraft.core.item.ItemStack;
import net.minecraft.core.world.World;
import com.beetmacol.mc.btir.item.BtirItems;

public class SulfurOre extends Block {
	public SulfurOre(String key) {
		super(key, Main.IDMap.get(key), Material.stone);
	}

	@Override
	public ItemStack[] getBreakResult(World world, EnumDropCause dropCause, int x, int y, int z, int meta, TileEntity tileEntity) {
		switch (dropCause) {
			case SILK_TOUCH:
			case PICK_BLOCK: {
				return new ItemStack[]{new ItemStack(this)};
			}
			case EXPLOSION:
			case PROPER_TOOL: {
				return new ItemStack[]{new ItemStack(BtirItems.SULFUR, world.rand.nextInt(3) + 1)};
			}
		}
		return null;
	}
}
