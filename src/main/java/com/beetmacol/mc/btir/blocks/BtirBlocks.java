package com.beetmacol.mc.btir.blocks;

import net.minecraft.core.block.material.Material;
import turniplabs.halplibe.helper.BlockBuilder;
import net.minecraft.client.render.block.model.BlockModelStandard;
import net.minecraft.core.block.tag.BlockTags;
import net.minecraft.core.item.tool.ItemToolPickaxe;
import com.beetmacol.mc.btir.Main;

public class BtirBlocks {
	public static SulfurOre SULFUR_ORE;
	public static Boiler BOILER;
	public static PipeBlock PIPE;
	public static SulfurBlock SULFUR_BLOCK;
	public static CreativeGasWell CREATIVE_GAS_WELL;

	public static void registerBlocks() {
		SULFUR_ORE = new BlockBuilder(Main.MOD_ID)
			.setBlockModel(block -> new BlockModelStandard<>(block).withTextures(Main.MOD_ID + ":block/sulfur_ore"))
			.setInfiniburn()
			.setHardness(3.0f)
			.setTags(BlockTags.MINEABLE_BY_PICKAXE)
			.build(new SulfurOre("sulfur_ore"));
		ItemToolPickaxe.miningLevels.put(SULFUR_ORE, 1);

		SULFUR_BLOCK = new BlockBuilder(Main.MOD_ID)
			.setFlammability(80, 10)
			.setBlockModel(block -> new BlockModelStandard<>(block).withTextures(Main.MOD_ID + ":block/sulfur_block"))
			.build(new SulfurBlock("sulfur_block", Material.metal));

		BOILER = new BlockBuilder(Main.MOD_ID)
			.setBlockModel(block -> new BlockModelStandard<>(block).withTextures(Main.MOD_ID + ":block/boiler_top", Main.MOD_ID + ":block/boiler_bottom", Main.MOD_ID + ":block/boiler_side"))
			.build(new Boiler("boiler", Material.metal));

		PIPE = new BlockBuilder(Main.MOD_ID)
			.setBlockModel(block -> new BlockModelStandard<>(block).withTextures(Main.MOD_ID + ":block/pipe"))
			.build(new PipeBlock("pipe", Material.metal));

		CREATIVE_GAS_WELL = new BlockBuilder(Main.MOD_ID)
			.setBlockModel(block -> new BlockModelStandard<>(block).withTextures(Main.MOD_ID + ":block/pipe", "minecraft:block/sponge"))
			.build(new CreativeGasWell("creative_gas_well", Material.metal));
	}
}
