package com.beetmacol.mc.btir.blocks;

import com.beetmacol.mc.btir.Main;
import net.minecraft.core.block.Block;
import net.minecraft.core.block.material.Material;

public class SulfurBlock extends Block {
	public SulfurBlock(String key, Material material) {
		super(key, Main.IDMap.get(key), material);
	}
}
