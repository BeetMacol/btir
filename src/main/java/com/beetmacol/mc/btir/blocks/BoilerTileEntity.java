package com.beetmacol.mc.btir.blocks;

import com.mojang.nbt.CompoundTag;
import com.mojang.nbt.ListTag;
import net.minecraft.core.block.entity.TileEntity;
import net.minecraft.core.entity.player.EntityPlayer;
import net.minecraft.core.item.ItemStack;

public class BoilerTileEntity extends TileEntity {
	public int fuel_count = 0;

	@Override
	public void readFromNBT(CompoundTag nbttagcompound) {
		super.readFromNBT(nbttagcompound);
		this.fuel_count = nbttagcompound.getIntegerOrDefault("FuelCount", 0);
	}

	@Override
	public void writeToNBT(CompoundTag nbttagcompound) {
		super.writeToNBT(nbttagcompound);
		nbttagcompound.putInt("FuelCount", this.fuel_count);
	}

	public void addFuel(EntityPlayer player, ItemStack stack, int amount) {
		stack.consumeItem(player);
		fuel_count += amount;
	}
}
