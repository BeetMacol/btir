package com.beetmacol.mc.btir.blocks;

import com.beetmacol.mc.btir.Main;
import net.minecraft.core.block.BlockTileEntity;
import net.minecraft.core.block.entity.TileEntity;
import net.minecraft.core.block.material.Material;
import net.minecraft.core.entity.player.EntityPlayer;
import net.minecraft.core.item.Item;
import net.minecraft.core.item.ItemStack;
import net.minecraft.core.world.World;

import java.util.Random;

public class Boiler extends BlockTileEntity {
	public Boiler(String key, Material material) {
		super(key, Main.IDMap.get(key), material);
		this.setTicking(true);
		TileEntity.addMapping(BoilerTileEntity.class, "BtirBoiler");
	}

	@Override
	public int tickRate() {
		return 20;
	}

	@Override
	public void onBlockAdded(World world, int x, int y, int z) {
		super.onBlockAdded(world, x, y, z);
		BoilerTileEntity tileEntity = (BoilerTileEntity)world.getBlockTileEntity(x, y, z);
		if(tileEntity.fuel_count > 0) world.scheduleBlockUpdate(x, y, z, this.id, tickRate());
	}

	@Override
	public void updateTick(World world, int x, int y, int z, Random rand) {
		BoilerTileEntity tileEntity = (BoilerTileEntity)world.getBlockTileEntity(x, y, z);
		if(tileEntity.fuel_count > 0) {
			//TODO: Boiler functionality
			tileEntity.fuel_count--;
			world.scheduleBlockUpdate(x, y, z, this.id, this.tickRate());
		}
	}


	@Override
	protected TileEntity getNewBlockEntity() {
		return new BoilerTileEntity();
	}

	@Override
	public boolean blockActivated(World world, int x, int y, int z, EntityPlayer player) {
		if (!world.isClientSide) {
			BoilerTileEntity tileEntity = (BoilerTileEntity)world.getBlockTileEntity(x, y, z);
			ItemStack held_item = player.inventory.getCurrentItem();
			if (held_item != null) {
				if (held_item.getItem() == Item.coal) tileEntity.addFuel(player, held_item, 40);
				else if (held_item.getItem() == Item.nethercoal) tileEntity.addFuel(player, held_item, 80);
				world.scheduleBlockUpdate(x, y, z, BtirBlocks.BOILER.id, tickRate());
			}
		}

		return true;
	}

	@Override
	public void randomDisplayTick(World world, int x, int y, int z, Random rand) {
		BoilerTileEntity tileEntity = (BoilerTileEntity)world.getBlockTileEntity(x, y, z);
		if (tileEntity.fuel_count > 0) {
			double poxX = (double)x + 0.5;
			double posY = (double)y + 0.0 + (double)(rand.nextFloat() * 6.0F / 16.0F);
			double posZ = (double)z + 0.5;
			double f3 = 0.5199999809265137;
			double f4 = (double)(rand.nextFloat() * 0.6F - 0.3F);
			long l = rand.nextInt(4);
			if (l == 0) {
				world.spawnParticle("smoke", poxX - f3, posY, posZ + f4, 0.0, 0.0, 0.0, 0);
				world.spawnParticle("flame", poxX - f3, posY, posZ + f4, 0.0, 0.0, 0.0, 0);
			} else if (l == 1) {
				world.spawnParticle("smoke", poxX + f3, posY, posZ + f4, 0.0, 0.0, 0.0, 0);
				world.spawnParticle("flame", poxX + f3, posY, posZ + f4, 0.0, 0.0, 0.0, 0);
			} else if (l == 2) {
				world.spawnParticle("smoke", poxX + f4, posY, posZ - f3, 0.0, 0.0, 0.0, 0);
				world.spawnParticle("flame", poxX + f4, posY, posZ - f3, 0.0, 0.0, 0.0, 0);
			} else if (l == 3) {
				world.spawnParticle("smoke", poxX + f4, posY, posZ + f3, 0.0, 0.0, 0.0, 0);
				world.spawnParticle("flame", poxX + f4, posY, posZ + f3, 0.0, 0.0, 0.0, 0);
			}
		}
	}
}
