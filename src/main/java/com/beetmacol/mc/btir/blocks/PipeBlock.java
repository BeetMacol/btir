package com.beetmacol.mc.btir.blocks;

import com.beetmacol.mc.btir.Main;
import com.beetmacol.mc.btir.gases.GasBlock;
import net.minecraft.core.block.BlockTileEntity;
import net.minecraft.core.block.entity.TileEntity;
import net.minecraft.core.block.material.Material;
import net.minecraft.core.util.helper.Direction;
import net.minecraft.core.world.World;

import java.util.Random;

public class PipeBlock extends BlockTileEntity {
	static final float MAX_PREASURE = 10.0f;
	static final int TICK_RATE = 10;

	public PipeBlock(String key, Material material) {
		super(key, Main.IDMap.get(key), material);
		this.setTicking(true);
			TileEntity.addMapping(PipeTileEntity.class, "BtirPipe");
	}

	@Override
	public int tickRate() {
		return TICK_RATE;
	}

	@Override
	protected TileEntity getNewBlockEntity() {
		return new PipeTileEntity();
	}

	@Override
	public void onBlockAdded(World world, int x, int y, int z) {
		super.onBlockAdded(world, x, y, z);
		PipeTileEntity tileEntity = (PipeTileEntity)world.getBlockTileEntity(x, y, z);
		if(tileEntity.preasure > 0) world.scheduleBlockUpdate(x, y, z, this.id, tickRate());
	}

	@Override
	public void updateTick(World world, int x, int y, int z, Random rand) {
		boolean isPoweredByBlock = world.isBlockGettingPowered(x, y, z);
		if(isPoweredByBlock) return;
		PipeTileEntity tileEntity = (PipeTileEntity)world.getBlockTileEntity(x, y, z);
		for(Direction dir : Direction.directions) {
			int xx = x + dir.getOffsetX();
			int yy = y + dir.getOffsetY();
			int zz = z + dir.getOffsetZ();
			if(world.getBlockId(xx, yy, zz) == this.id) {
				PipeTileEntity otherPipe = (PipeTileEntity)world.getBlockTileEntity(xx, yy, zz);
				if ((otherPipe.gas_id == tileEntity.gas_id) || (otherPipe.gas_id == 0)) {
					if (tileEntity.preasure > otherPipe.preasure) {
						float preasure_diff = tileEntity.preasure - otherPipe.preasure;
						if(insertGas(world, xx, yy, zz, tileEntity.gas_id, preasure_diff)) {
							otherPipe.gas_id = tileEntity.gas_id;
							tileEntity.preasure -= preasure_diff;
						}
					}
				}
			}
		}
		if(tileEntity.preasure > 0.0f) world.scheduleBlockUpdate(x, y, z, this.id, this.tickRate());
	}

	public static boolean insertGas(World world, int x, int y, int z, int gas_id, float amount) {
		if(world.getBlockId(x, y, z) == BtirBlocks.PIPE.id) {
			PipeTileEntity tileEntity = (PipeTileEntity)world.getBlockTileEntity(x, y, z);
			if((tileEntity.gas_id == 0) || (tileEntity.gas_id == gas_id)) {
				tileEntity.gas_id = gas_id;
				if((tileEntity.preasure + amount) > MAX_PREASURE) return false;
				tileEntity.preasure += amount;
				world.scheduleBlockUpdate(x, y, z, BtirBlocks.PIPE.id, TICK_RATE);
				return true;
			}
		}
		return false;
	}
	public static boolean insertGasBlock(World world, int x, int y, int z, int gas_id) {
		if(!insertGas(world, x, y, z, gas_id, 1.0f)) {
			if(world.getBlockId(x, y, z) == 0) {
				world.setBlock(x, y, z, gas_id);
			} else return false;
		}
		return true;
	}

	@Override
	public void onBlockRemoved(World world, int x, int y, int z, int data) {
		PipeTileEntity tileEntity = (PipeTileEntity)world.getBlockTileEntity(x, y, z);
		if(tileEntity.preasure > 0.0f) {
			int gas_blocks = Math.round(tileEntity.preasure);
			GasBlock.makeGasClound(world, x, y, z, tileEntity.gas_id, gas_blocks, 1);
		}
	}

	@Override
	public void onNeighborBlockChange(World world, int x, int y, int z, int blockId) {
		world.scheduleBlockUpdate(x, y, z, this.id, this.tickRate());
	}
}
