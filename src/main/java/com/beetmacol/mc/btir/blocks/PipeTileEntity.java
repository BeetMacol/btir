package com.beetmacol.mc.btir.blocks;

import com.mojang.nbt.CompoundTag;
import net.minecraft.core.block.entity.TileEntity;

public class PipeTileEntity extends TileEntity {
	public float preasure = 0.0f;
	public int gas_id = 0;

	@Override
	public void readFromNBT(CompoundTag nbttagcompound) {
		super.readFromNBT(nbttagcompound);
		this.preasure = nbttagcompound.getFloatOrDefault("Preasure", 0.0f);
		this.gas_id = nbttagcompound.getIntegerOrDefault("GasID", 0);
	}

	@Override
	public void writeToNBT(CompoundTag nbttagcompound) {
		super.writeToNBT(nbttagcompound);
		nbttagcompound.putFloat("Preasure", this.preasure);
		nbttagcompound.putInt("GasID", this.gas_id);
	}
}
