package com.beetmacol.mc.btir.blocks;

import com.beetmacol.mc.btir.Main;
import com.beetmacol.mc.btir.gases.BtirGases;
import net.minecraft.core.block.Block;
import net.minecraft.core.block.material.Material;
import net.minecraft.core.world.World;

import java.nio.channels.Pipe;
import java.util.Random;

public class CreativeGasWell extends Block {
	public CreativeGasWell(String key, Material material) {
		super(key, Main.IDMap.get(key), material);
		this.setTicking(true);
	}

	@Override
	public int tickRate() {
		return 20;
	}

	@Override
	public void onBlockAdded(World world, int x, int y, int z) {
		super.onBlockAdded(world, x, y, z);
		world.scheduleBlockUpdate(x, y, z, this.id, this.tickRate());
	}

	@Override
	public void updateTick(World world, int x, int y, int z, Random rand) {
		boolean isPoweredByBlock= world.isBlockGettingPowered(x, y, z) || world.isBlockIndirectlyGettingPowered(x, y, z);
		if (isPoweredByBlock) {
			PipeBlock.insertGasBlock(world, x, y + 1, z, BtirGases.SULFUR_DIOXIDE.id);
			world.scheduleBlockUpdate(x, y, z, this.id, this.tickRate());
		}
	}

	@Override
	public void onNeighborBlockChange(World world, int x, int y, int z, int blockId) {
		world.scheduleBlockUpdate(x, y, z, this.id, this.tickRate());
	}
}
