package com.beetmacol.mc.btir.item;

import com.beetmacol.mc.btir.Main;
import net.minecraft.core.item.Item;

public class Sulfur extends Item {
	public Sulfur(String key) {
		super(key, Main.IDMap.get(key));
	}
}
