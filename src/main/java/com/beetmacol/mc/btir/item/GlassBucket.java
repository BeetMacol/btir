package com.beetmacol.mc.btir.item;

import net.minecraft.core.block.Block;
import net.minecraft.core.entity.player.EntityPlayer;
import net.minecraft.core.item.ItemBucket;
import net.minecraft.core.item.ItemStack;
import net.minecraft.core.world.World;

public class GlassBucket extends ItemBucket {
	public GlassBucket(String name, int id, Block blockToPlace) {
		super(name, id, blockToPlace);
	}

	@Override
	public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player) {
		return super.onItemRightClick(stack, world, player);
	}
}
