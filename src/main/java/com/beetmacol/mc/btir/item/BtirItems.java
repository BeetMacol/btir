package com.beetmacol.mc.btir.item;

import turniplabs.halplibe.helper.ItemBuilder;
import com.beetmacol.mc.btir.Main;

public class BtirItems {
	public static Sulfur SULFUR;

	public static void registerItems() {
		SULFUR = new ItemBuilder(Main.MOD_ID)
			.setIcon("btir:item/sulfur")
			.build(new Sulfur("sulfur"));
	}
}
