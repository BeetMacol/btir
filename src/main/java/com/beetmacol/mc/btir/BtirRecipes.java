package com.beetmacol.mc.btir;

import com.beetmacol.mc.btir.blocks.BtirBlocks;
import com.beetmacol.mc.btir.item.BtirItems;
import net.minecraft.core.block.Block;
import net.minecraft.core.item.Item;
import net.minecraft.core.item.ItemStack;
import turniplabs.halplibe.helper.recipeBuilders.RecipeBuilderFurnace;
import turniplabs.halplibe.helper.recipeBuilders.RecipeBuilderShaped;
import turniplabs.halplibe.helper.recipeBuilders.RecipeBuilderShapeless;

public class BtirRecipes {
	public static void registerRecipes() {
		new RecipeBuilderFurnace(Main.MOD_ID).setInput(BtirBlocks.SULFUR_ORE).create("sulfure_ore_smelting", BtirItems.SULFUR.getDefaultStack());
		new RecipeBuilderShaped(Main.MOD_ID, "SSS", "SGS", "SFS").addInput('S', Item.ingotSteel).addInput('G', Block.glass).addInput('F', Block.furnaceStoneIdle).create("boiler", BtirBlocks.BOILER.getDefaultStack());
		new RecipeBuilderShaped(Main.MOD_ID, "SSS", "SSS", "SSS").addInput('S', BtirItems.SULFUR).create("sulfur_block", BtirBlocks.SULFUR_BLOCK.getDefaultStack());
		new RecipeBuilderShapeless(Main.MOD_ID).addInput(BtirBlocks.SULFUR_BLOCK).create("sulfur_block_undo", new ItemStack(BtirItems.SULFUR, 9));
	}
}
