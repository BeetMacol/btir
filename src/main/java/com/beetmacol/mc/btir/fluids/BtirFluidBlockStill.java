package com.beetmacol.mc.btir.fluids;

import net.minecraft.core.block.BlockFluidStill;
import net.minecraft.core.block.material.Material;

public class BtirFluidBlockStill extends BlockFluidStill {
	public final int tickRate;

	public BtirFluidBlockStill(String key, int id, Material material, int tickRate) {
		super(key, id, material);
		this.tickRate = tickRate;
	}

	@Override
	public int tickRate() {
		return this.tickRate;
	}
}
