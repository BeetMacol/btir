package com.beetmacol.mc.btir.fluids;

import net.minecraft.core.block.BlockFluidFlowing;
import net.minecraft.core.block.BlockFluidStill;
import net.minecraft.core.block.material.Material;

public class BtirFluidBlockFlowing extends BlockFluidFlowing {
	public final int tickRate;

	public BtirFluidBlockFlowing(String key, int id, Material material, int tickRate) {
		super(key, id, material);
		this.tickRate = tickRate;
	}

	@Override
	public int tickRate() {
		return this.tickRate;
	}
}
