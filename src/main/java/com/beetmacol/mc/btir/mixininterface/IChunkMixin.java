package com.beetmacol.mc.btir.mixininterface;

public interface IChunkMixin {
	void setPollution(float value);
	float getPollution();
}
