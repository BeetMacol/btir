package com.beetmacol.mc.btir.gases;

import com.beetmacol.mc.btir.Main;
import com.beetmacol.mc.btir.mixininterface.IChunkMixin;
import net.minecraft.core.block.BlockTransparent;
import net.minecraft.core.block.material.Material;
import net.minecraft.core.entity.Entity;
import net.minecraft.core.util.helper.DamageType;
import net.minecraft.core.util.helper.Side;
import net.minecraft.core.util.phys.AABB;
import net.minecraft.core.world.World;
import net.minecraft.core.world.WorldSource;

import java.util.ArrayList;
import java.util.Random;

public class GasBlock extends BlockTransparent {
	public static int tickRate;
	public static boolean deadly;

	public GasBlock(String key, Material material, int tickRate, boolean deadly) {
		super(key, Main.IDMap.get(key), material);
		this.setTicking(true);
		this.tickRate = tickRate;
		this.deadly = deadly;
	}

	@Override
	public int tickRate() {
		return tickRate;
	}

	@Override
	public void onBlockAdded(World world, int x, int y, int z) {
		super.onBlockAdded(world, x, y, z);
		world.scheduleBlockUpdate(x, y, z, this.id, tickRate());
	}

	@Override
	public void onNeighborBlockChange(World world, int x, int y, int z, int blockId) {
		world.scheduleBlockUpdate(x, y, z, this.id, this.tickRate());
	}

	public boolean gasMove(World world, int x, int y, int z, int nx, int ny, int nz) {
		if (world.getBlockId(nx, ny, nz) == 0) {
			world.setBlock(nx, ny, nz, this.id);
			world.setBlock(x, y, z, 0);
			if (ny > /*world.dimension.defaultWorldType.getMaxY()*/ 250) this.onLeaveAtmosphere(world, nx, ny, nz);
			return true;
		} else return false;
	}

	@Override
	public void updateTick(World world, int ox, int oy, int oz, Random rand) {
		int x = ox;
		int y = oy;
		int z = oz;
		boolean success = gasMove(world, x, y, z, x, y + 1, z);
		if (success) y++;
		if (!success || rand.nextInt(10) > 8) {
			int hdir = 2 + rand.nextInt(4);
			Side side = Side.getSideById(hdir);
			boolean hsuccess = gasMove(world, x, y, z, x + side.getOffsetX(), y, z + side.getOffsetZ());
			if (hsuccess) {
				success = true;
				x += side.getOffsetX();
				z += side.getOffsetZ();
			}
		}
		if (success) world.notifyBlockChange(ox, oy, oz, this.id);
		else world.scheduleBlockUpdate(x, y, z, this.id, this.tickRate());
	}

	@Override
	public boolean collidesWithEntity(Entity entity, World world, int x, int y, int z) {
 		return false;
	}

	@Override
	public void onEntityCollidedWithBlock(World world, int x, int y, int z, Entity entity) {
		if(this.deadly) {
			entity.hurt(null,1, DamageType.DROWN);
		}
		super.onEntityCollidedWithBlock(world, x, y, z, entity);
	}

	@Override
	public int getRenderBlockPass() {
		return 1;
	}

	@Override
	public boolean isCollidable() {
		return false;
	}

	@Override
	public boolean canCollideCheck(int meta, boolean shouldCollideWithFluids) {
		return false;
	}

	@Override
	public boolean getIsBlockSolid(WorldSource blockAccess, int x, int y, int z, Side side) {
		return false;
	}

	@Override
	public void getCollidingBoundingBoxes(World world, int x, int y, int z, AABB aabb, ArrayList<AABB> aabbList) {
		super.getCollidingBoundingBoxes(world, x, y, z, aabb, aabbList);
	}

	public void onLeaveAtmosphere(World world, int x, int y, int z) {
		world.setBlock(x, y, z, 0);
		IChunkMixin chunkMixin = ((IChunkMixin)world.getChunkFromBlockCoords(x, z));
		float pollution = chunkMixin.getPollution();
		chunkMixin.setPollution(pollution + 0.05f);
	}

	static final int MAX_CLOUND_ITERATIONS = 100;

	public static void makeGasClound(World world, int x, int y, int z, int id, int count, int max_range) {
		int iter = 0;
		while (iter < MAX_CLOUND_ITERATIONS) {
			if(count == 0) return;
			int xx = (x + world.rand.nextInt((max_range * 2) + 1) - max_range);
			int yy = (y + world.rand.nextInt((max_range * 2) + 1) - max_range);
			int zz = (z + world.rand.nextInt((max_range * 2) + 1) - max_range);
			if(world.getBlockId(xx, yy, zz) == 0) {
				world.setBlock(xx, yy, zz, id);
				count--;
			}
			iter++;
		}
	}
}
