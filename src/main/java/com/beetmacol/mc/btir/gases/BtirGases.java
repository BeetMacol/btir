package com.beetmacol.mc.btir.gases;

import com.beetmacol.mc.btir.Main;
import com.beetmacol.mc.btir.blocks.SulfurOre;
import net.minecraft.client.render.block.model.BlockModelStandard;
import net.minecraft.core.block.material.Material;
import net.minecraft.core.block.material.MaterialColor;
import net.minecraft.core.block.tag.BlockTags;
import turniplabs.halplibe.helper.BlockBuilder;

public class BtirGases {
	public static Material GAS_MATERIAL = new Material(MaterialColor.none).notSolidBlocking().destroyOnPush();
	public static GasBlock SULFUR_DIOXIDE;

	// TODO material must be notSolidBlocking

	public static void registerGases() {
		SULFUR_DIOXIDE = new BlockBuilder(Main.MOD_ID).setBlockModel(block -> new BlockModelStandard<>(block).withTextures(Main.MOD_ID + ":block/sulfur_dioxide"))
			.build(new GasBlock("sulfur_dioxide", GAS_MATERIAL, 10, true));
	}
}
