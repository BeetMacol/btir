package com.beetmacol.mc.btir;

import net.fabricmc.api.ModInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import turniplabs.halplibe.util.GameStartEntrypoint;
import turniplabs.halplibe.util.RecipeEntrypoint;
import com.beetmacol.mc.btir.blocks.BtirBlocks;
import com.beetmacol.mc.btir.fluids.BtirFluids;
import com.beetmacol.mc.btir.gases.BtirGases;
import com.beetmacol.mc.btir.item.BtirItems;

import java.util.HashMap;
import java.util.Map;

public class Main implements ModInitializer, GameStartEntrypoint, RecipeEntrypoint {
    public static final String MOD_ID = "btir";
    public static final Logger LOGGER = LoggerFactory.getLogger(MOD_ID);
	public static Map<String, Integer> IDMap = new HashMap<>();

	private static int base_id = 6300;

	private void id_mapping(String key, int offset) { IDMap.put(key, base_id + offset); }

	@Override
	public void onInitialize() {
		setupIDMappings();
		BtirBlocks.registerBlocks();
		BtirItems.registerItems();
		BtirFluids.registerFluids();
		BtirGases.registerGases();
	}

	@Override
	public void beforeGameStart() {
	}

	@Override
	public void afterGameStart() {
	}

	@Override
	public void onRecipesReady() {
		BtirRecipes.registerRecipes();
	}

	@Override
	public void initNamespaces() {
	}

	private void setupIDMappings() {
		id_mapping("boiler", 0);
		id_mapping("pipe", 1);
		id_mapping("sulfur_block", 2);
		id_mapping("sulfur_ore", 3);
		id_mapping("sulfur_dioxide", 4);
		id_mapping("sulfur", 5);
		id_mapping("creative_gas_well", 6);
	}
}
