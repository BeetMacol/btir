package com.beetmacol.mc.btir.mixin;

import com.beetmacol.mc.btir.mixininterface.IChunkMixin;
import com.mojang.nbt.CompoundTag;
import net.minecraft.core.world.World;
import net.minecraft.core.world.chunk.Chunk;
import net.minecraft.core.world.chunk.ChunkLoaderLegacy;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(ChunkLoaderLegacy.class)
public class ChunkLoaderLegacyMixin {
	@Inject(
		method = "storeChunkInCompound",
		at = @At("TAIL"),
		remap = false
	)
	private static void onStoreChunkInCompound(Chunk chunk, World world, CompoundTag tag, CallbackInfo ci) {
		tag.putFloat("Pollution", ((IChunkMixin)chunk).getPollution());
	}

	@Inject(
		method = "loadChunkIntoWorldFromCompound",
		at = @At("TAIL"),
		remap = false
	)
	private static void onLoadChunkIntoWorldFromCompound(World world, CompoundTag tag, CallbackInfoReturnable<Chunk> cir) {
		float pollution = tag.getFloatOrDefault("Pollution", 0.0f);
		System.out.println("Pollution: " + pollution);
		Chunk chunk = cir.getReturnValue();
		((IChunkMixin)chunk).setPollution(pollution);
	}
}
