package com.beetmacol.mc.btir.mixin;

import com.beetmacol.mc.btir.blocks.BtirBlocks;
import com.beetmacol.mc.btir.gases.BtirGases;
import net.minecraft.core.block.Block;
import net.minecraft.core.block.BlockFire;
import net.minecraft.core.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.Random;

@Mixin(BlockFire.class)
public class BlockFireMixin {
	@Inject(
		method="updateTick",
		at = @At("TAIL"),
		remap = false
	)
	private void onUpdateTick(World world, int x, int y, int z, Random rand, CallbackInfo ci) {
		if(world.getBlockId(x, y - 1, z) == BtirBlocks.SULFUR_BLOCK.id) {
			if(world.getBlockId(x, y + 1, z) == 0) {
				world.setBlock(x, y + 1, z, BtirGases.SULFUR_DIOXIDE.id);
			}
		}
	}
}
