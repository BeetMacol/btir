package com.beetmacol.mc.btir.mixin;

import java.util.Random;
import net.minecraft.core.block.Block;
import net.minecraft.core.world.World;
import net.minecraft.core.world.chunk.Chunk;
import net.minecraft.core.world.generate.chunk.perlin.nether.ChunkDecoratorNether;
import net.minecraft.core.world.generate.feature.WorldFeatureOre;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;
import com.beetmacol.mc.btir.blocks.BtirBlocks;

@Mixin(ChunkDecoratorNether.class)
public class ChunkDecoratorNetherMixin {
	@Shadow
	@Final
	private World world;

	@Inject(
		method = "decorate", // (Lnet.minecraft.core.world.chunk.Chunk;)V
		at = @At("TAIL"),
//		locals = LocalCapture.CAPTURE_FAILEXCEPTION,
		remap = false
	)
	private void onDecorate(Chunk chunk, CallbackInfo ci) { // FIXME capture rand
		Random rand = new Random((long) chunk.xPosition * 341872871112L + (long) chunk.zPosition * 132709987541L);
		int minY = this.world.getWorldType().getMinY();
		int maxY = 165;
		int rangeY = maxY + 1 - minY;
		for (int i = 0; i < 1; ++i) {
			int xf2 = chunk.xPosition * 16 + rand.nextInt(16);
			int yf2 = minY + rand.nextInt(rangeY - 8) + 4;
			int zf2 = chunk.zPosition * 16 + rand.nextInt(16);
			new WorldFeatureOre(BtirBlocks.SULFUR_ORE.id, 14, false).generate(this.world, rand, xf2, yf2, zf2);
		}
	}
}
