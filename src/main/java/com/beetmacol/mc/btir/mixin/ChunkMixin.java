package com.beetmacol.mc.btir.mixin;

import com.beetmacol.mc.btir.mixininterface.IChunkMixin;
import net.minecraft.core.world.chunk.Chunk;
import org.spongepowered.asm.mixin.Mixin;

@Mixin(Chunk.class)
public class ChunkMixin implements IChunkMixin {
	public float pollution = 0.0f;

	@Override
	public void setPollution(float value) {
		pollution = value;
	}

	@Override
	public float getPollution() {
		return pollution;
	}
}
